<?php
$PATH_TO_DATABASE = '..\test\database\db.xml';
$MD5_KEY = 'соль';


function check_xml($path, $login, $email)
{
	$xml = simplexml_load_file($path);
   		foreach ($xml as $account) 
   		{
			if($account->login == $login)
			{
				echo "This account already in use";
				return false;
			}
			else if ($account->email == $email) 
			{
				echo "This email already in use";
				return false;
			}
		}
	return true;	
}


if(isset($_POST['login']) && isset($_POST['password']) && 
    isset($_POST['conf_pass']) && isset($_POST['email']) && isset($_POST['name']))
{
	$login = htmlentities($_POST['login']);
    $password = htmlentities($_POST['password']);
    $confirm_password = htmlentities($_POST['conf_pass']);
    $email = htmlentities($_POST['email']);
    $name = htmlentities($_POST['name']);

    if($password != $confirm_password)
    {
    	echo "Passwords do not match";
    }
	else
	{
		if(check_xml($PATH_TO_DATABASE, $login, $email))
		{

			$xml = simplexml_load_file($PATH_TO_DATABASE);
			$new_password = md5($password . $MD5_KEY);

			$new_account = $xml -> addChild("account");
			$new_account -> addChild("login", $login);
			$new_account -> addChild("password", $new_password);
			$new_account -> addChild("email", $email);
			$new_account -> addChild("name", $name);


			$new_xml = fopen($PATH_TO_DATABASE, 'w');
			fwrite($new_xml,$xml->asXML());
			fclose($new_xml);
			echo "Success";
		}
		return;
	}
}
else {
	echo "Please, fill in all the fields";
}


?>