<?php
$PATH_TO_DATABASE = '..\test\database\db.xml';


if (empty($_SESSION['auth']) or $_SESSION['auth'] == false) {
	if ( !empty($_COOKIE['login']) and !empty($_COOKIE['key']) ) {
		$login = $_COOKIE['login']; 
		$key = $_COOKIE['key'];

		$xml = simplexml_load_file($PATH_TO_DATABASE);
		foreach ($xml as $account) 
    	{
			if($account->login == $login) 
				if($account->key == $key)
				{
					session_start(); 
					$_SESSION['auth'] = true; 

					$_SESSION['login'] = $account['login']; 
					return true;
				}

		}
	}
	return false;
}
else
{
	return true;
}
?>