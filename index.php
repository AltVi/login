<!DOCTYPE html>
<html>
<head>
	<title>Log in</title>
	<link rel="stylesheet" type="text/css" href="style/main.css"></link>
	<script type="text/javascript" src="scrypts/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="scrypts/tabs.js"></script>
	<script type="text/javascript" src="scrypts/ajax.js"></script>
</head>
<body>
	<noscript>
        <p>please, unlock Javascript.</p>
        <img width="700px" height="500px" id="ban" src="image/404_page.png">
    </noscript>
	<header>
		<?php include ("check.php");
		include ("blocks/header.php");?>
	</header>

	<main>
		<div id="wrapper">
			<div class="tab">
  				<button class="tablinks" onclick="openTab(event, 'regist')">Registration</button>
  				<button class="tablinks" onclick="openTab(event, 'auth')">Authorization</button>
			</div>
			<div id="regist" class="tabcontent">
				<form class="signin" id="reg_form" action="/registration.php" method="post">
					<input type="text" id="login" name="login" placeholder="login" required />
					<input type="password" id="pass" name="password" placeholder="password" required />
					<input type="password" id="conf_pass" name="conf_pass" placeholder="confirm password" required />
					<input type="text" id="email" name="email" placeholder="email" required />
					<input type="text" id="name" name="name" placeholder="name" required />
					<button type="submit" id="btn_reg" name="btn_reg"><b>>></b></button>	
				</form>
			</div>
			<div id="auth" class="tabcontent">
				<form class="signin" id="au_form" action="/authorization.php" method="post">
					<input type="text" id="login_au" name="login_au" placeholder="login" />
					<input type="password" id="password_au" name="password_au" placeholder="password" />
					<button type="submit" id="btn_au" name="btn_au"><b>>></b></button>
				</form>
			</div>
		</div>	
	</main>

	<footer>
		<p>Reit me <a href="https://bitbucket.org/AltVi/" target="_blank">click here</a></p>
	</footer>
</body>
</html>